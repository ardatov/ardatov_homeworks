import java.util.ArrayList;
import java.util.Scanner;

public class homework07 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int[] mass = new int[201];
        int result = 0;
        int min = Integer.MAX_VALUE;
        ArrayList<Integer> array = new ArrayList<>();
        int num = in.nextInt();
        while (num != -1) {
            array.add(num);
            num = in.nextInt();
        }

        for (int i = 0; i < array.size(); i++) {
            mass[array.get(i) + 100]++;
        }
        for (int j = 0; j < mass.length; j++) {
            if (mass[j] != 0) {
                if (mass[j] < min) {
                    min = mass[j];
                    result = j;
                }
            }
        }
        System.out.println(result - 100);
    }
}