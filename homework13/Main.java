package homework13;

public interface ByCondition {
    boolean isOk(int number);
}

package homework13;

public class Sequence {
    public static int[] filter(int[] array, ByCondition condition) {
        int[] temp = new int[array.length];
        for (int i = 0; i < array.length; i++){
            if(condition.isOk(array[i])){
                temp[i] = array[i];
            }
            else temp[i] = -1;

        }
        int kolElem = 0;
        for (int i : temp){
            if( i == -1) kolElem++;
        }
        int[] vozv = new int[temp.length - kolElem];
        int i = 0;
        for(int j : temp){
            if (j != -1){
                vozv[i] = j;
                i++;
            }
        }


        return vozv;
    }

}

package homework13;

        import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] mass = {1,25,10,11,15,28,14,100,50,51};
        int[] a = Sequence.filter(mass,i -> i%2 == 0);
        System.out.println(Arrays.toString(mass));
        System.out.println(Arrays.toString(a));
        int[] b = Sequence.filter(mass, number -> {
            int summ = 0;
            while (number / 10 !=0){
                summ += number % 10;
                number /=10;
            }
            summ +=number;
            return summ % 2 == 0;
        });
        System.out.println("------------------------------------");
        System.out.println(Arrays.toString(mass));
        System.out.println(Arrays.toString(b));


    }
}