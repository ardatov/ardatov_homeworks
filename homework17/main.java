package homework17;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class homework17 {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        Scanner scanner = new Scanner(System.in);
        String[] stroka = scanner.nextLine().split(" ");
        for (String s : stroka) {
            if (map.containsKey(s)) {
                int temp = map.get(s);
                temp++;
                map.put(s, temp);
            } else map.put(s, 1);
        }

        for(Map.Entry<String, Integer> entry: map.entrySet())
            System.out.println(entry.getKey() + " - " + entry.getValue());
    }
}
