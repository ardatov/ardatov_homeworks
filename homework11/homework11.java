public class Logger {
    private static Logger logger;
    private Logger(){

    }

    public static Logger getLogger() {
        if (logger == null) synchronized (Logger.class){
            if (logger == null) logger = new Logger();
        }
        return logger;
    }

    void log(String message){
        System.out.println(message);
    }
}

public class homework11 {
    public static void homework11(String[] args) {
        Logger.getLogger().log("Привет я закрытый класс");

    }
}