create table product
(
    id serial primary key ,
    description varchar(255),
    price decimal(10, 2) check ( price >= 0 ),
    quantity integer check ( quantity >= 0 )
);

create table customer
(
    id serial primary key ,
    name varchar(255)
);

create table orders
(
    id serial primary key ,
    product_id integer not null ,
    customer_id integer not null ,
    order_date date not null ,
    order_quantity integer check ( order_quantity > 0 ),
    foreign key (product_id) references product (id),
    foreign key (customer_id) references customer (id)
);

insert into product(description, price, quantity)
values ('product_1', 1000, 50),
       ('product_2', 100, 5000),
       ('product_3', 3000, 500),
       ('product_4', 500, 1000),
       ('product_5', 5000, 100);

insert into customer(name)
values ('John Doe'),
       ('John Smith');

insert into orders(product_id, customer_id, order_date, order_quantity)
values (3, 2, '2021-12-06', 5),
       (1, 2, '2021-08-15', 10),
       (2, 1, '2021-05-11', 2),
       (1, 1, '2021-08-01', 14),
       (5, 1, '2021-09-25', 1);

-- Самый дорогой товар
select * from product
where price = (select max(price) from product);

-- Товары, которые никто не заказывал
select description from product
where id not in (select product_id from orders);

-- Общая сумма заказов для каждого товара
select description, sum(price * order_quantity) as sum from product p
    join orders o on p.id = o.product_id
group by p.id
order by sum desc;

-- Сколько товаров заказал каждый покупатель в августе 2021 года
select name, sum(order_quantity) from customer c
    join orders o on c.id = o.customer_id
where order_date between '2021-08-01' and '2021-08-31'
group by c.id;

-- Сколько товаров осталось
select description, (quantity - sum(o.order_quantity)) as quantity from product p
    join orders o on p.id = o.product_id
group by p.id
order by quantity;