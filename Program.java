import java.util.Scanner;

class Program {
    public static void main(String[] args) {          // объявляю объект для считывания чисел с консоли
        Scanner scanner = new Scanner(System.in);    // считываю первое число
        int a = scanner.nextInt();                  // объявляю переменные и задаю им начальные значения
        int digitsSum = 0;                         // сумма цифр текущего числа
        int minDigitsSum = a;                     // Вроде как логично...
        while (a != -1) {                        // пока не встретили -1
            digitsSum = 0;                      // указываем, что сумма цифр должна изначально быть равна нулю
                while (a != 0) {               // пока число не равно нулю
                    int lastDigit = a % 10;   // запоминаем последную цифру
                    digitsSum = lastDigit;   // откладываем цифру
                    if (digitsSum < minDigitsSum) {
                        minDigitsSum = digitsSum;
                    }                     // забываем про старую минимальную сумму и запоминаем новую
                    a = a / 10;         // отбрасываем от исходного числа последнюю цифру
                }
            a = scanner.nextInt();                           // на каждом шаге цикла считываем новое число из консоли
        }
        System.out.println("Result - " + minDigitsSum);     // выводим результат
    }
}

