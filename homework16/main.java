package homework16;

public class ArrayList <T> {
    private static final int DEFAULT_SIZE =10;
    private T[] elements;
    private int size;
    public ArrayList(){
        this.elements =(T[]) new Object[DEFAULT_SIZE];
        this.size = 0;
    }
    public void add(T element){
        if(isFullArray()){
            resize();
        }

        this.elements[size] = element;
        this.size++;

    }

    private void resize() {
        T[] oldElements = this.elements;
        this.elements =(T[]) new Object[oldElements.length + oldElements.length / 2];
        for (int i = 0; i < size; i++){
            this.elements[i] = oldElements[i];
        }
    }

    private boolean isFullArray() {
        return size == elements.length;
    }

    public T get(int index){
        if(isCorrectIndex(index)) return elements[index];
        else return null;
    }

    private boolean isCorrectIndex(int index) {
        return index >= 0 && index < size;
    }

    public void clear(){
        size = 0;
    }
    public void removeAt(int index){
        if (isCorrectIndex(index)){
            T[] oldElem = this.elements;
            this.elements = (T[]) new Object[size-1];
            int j = 0;
            for (int i = 0; i < size; i++){
                if (index == i) continue;
                this.elements[j] = oldElem[i];
                j++;
            }
            size--;

        }
    }
}

package homework16;

public class LinkedList <T>{
    private static class Node<T>{
        T value;
        Node<T> next;

        public Node(T value) {
            this.value = value;
        }
    }
    private Node<T> first;
    private Node<T> last;
    private int size;
    public void add(T element){
        Node<T> newNode= new Node<>(element);
        if (size == 0){
            first = newNode;

        }
        else {
            last.next = newNode;

        }
        last = newNode;
        size++;
    }
    public int size(){
        return size;
    }
    public void addToBegin(T element){
        Node<T> newNode = new Node<>(element);
        if (size == 0){
            last = newNode;
        }
        else {
            newNode.next = first;
        }
        first = newNode;
        size++;
    }
    public T get(int index){
        if (index > size) return null;
        Node<T> retur = first;
        for (int i = index; i > 0; i--){
            retur = retur.next;
        }


        return retur.value;
    }
}

package homework16;

public class main {
    public static void main(String[] args) {
        ArrayList<String> lines = new ArrayList<>();
        lines.add("0");
        lines.add("1");
        lines.add("2");
        lines.add("3");
        lines.add("4");
        lines.add("5");
        lines.add("6");
        lines.add("7");
        lines.add("8");
        System.out.println(lines.get(5));
        lines.removeAt(5);
        System.out.println(lines.get(5));

        LinkedList<Integer> list = new LinkedList<>();
        list.add(34);
        list.add(120);
        list.add(-10);
        list.add(11);
        list.add(50);
        list.add(100);
        list.add(99);

        System.out.println(list.get(5));


    }

}
