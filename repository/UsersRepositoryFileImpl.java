package repository;

public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
        }

public User findById(int id) {
        User user = null;

        Reader reader = null;
        BufferedReader bufferedReader = null;

        try {
        reader = new FileReader(fileName);
        bufferedReader = new BufferedReader(reader);
        String line = bufferedReader.readLine();

        while (line != null) {
        String[] parts = line.split("\\|");
        line = bufferedReader.readLine();
        int idUser = Integer.parseInt(parts[0]);
        if (idUser == id) {
        String nameUser = parts[1];
        int ageUser = Integer.parseInt(parts[2]);
        boolean isWorker = Boolean.parseBoolean(parts[3]);
        user = new User(idUser, nameUser, ageUser, isWorker);
        break;
        }
        }

        } catch (IOException e) {
        throw new IllegalArgumentException(e);
        } finally {
        if (reader != null) {
        try {
        reader.close();
        } catch (IOException ignore) {}
        }
        if (bufferedReader != null) {
        try {
        bufferedReader.close();
        } catch (IOException ignore) {}
        }
        }
        return user;
        }

public void update(User user) {
        List<String> modifiedLines = new ArrayList<>();

        Reader reader = null;
        BufferedReader bufferedReader = null;
        try {
        reader = new FileReader(fileName);
        bufferedReader = new BufferedReader(reader);
        String line = bufferedReader.readLine();
        while (line != null) {
        String[] parts = line.split("\\|");

        int idUser = Integer.parseInt(parts[0]);
        if (idUser == user.getId()) {
        line = user.getId() +"|" + user.getName() + "|" + user.getAge() + "|" + user.getIsWorker();
        }
        modifiedLines.add(line);

        line = bufferedReader.readLine();
        }



        Writer writer = new FileWriter(fileName);
        for(int i=0; i < modifiedLines.size(); i++) {
        writer.write(modifiedLines.get(i) + "\n");
        }
        writer.close();

        } catch (IOException e) {
        throw new IllegalArgumentException(e);
        } finally {
        if (reader != null) {
        try {
        reader.close();
        } catch (IOException ignore) {
        }
        }
        if (bufferedReader != null) {
        try {
        bufferedReader.close();
        } catch (IOException ignore) {
        }
        }
        }
        }
