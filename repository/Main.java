import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class UsersRepositoryFileImpl implements UsersRepository {

    private String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public User findById(int ID) {
        List<User> user = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            reader.lines()
                    .forEach(line -> {
                        String[] parts = line.split("\\|");
                        int id = Integer.parseInt(parts[0]);
                        String name = parts[1];
                        int age = Integer.parseInt(parts[2]);
                        boolean isWorker = Boolean.parseBoolean(parts[3]);
                        User newUser = new User(id, name, age, isWorker);
                        if (id == ID) {
                            user.add(newUser);
                        }
                    });
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
        return user.get(0);
    }

    @Override
    public void update(User user) {
        List<User> users = findAll();
        for (User value : users) {
            if (user.getID() == value.getID()) {
                value.setName(user.getName());
                value.setAge(user.getAge());
                value.setWorker(user.isWorker());
            }
        }

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));) {
            users.forEach((line) -> {
                try {
                    writer.write(line.getID() + "|" + line.getName() + "|" + line.getAge() + "|" + line.isWorker());
                    writer.newLine();
                    writer.flush();
                } catch (IOException e) {
                    throw new IllegalArgumentException();
                }
            });
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        // объявили переменные для доступа
        Reader reader = null;
        BufferedReader bufferedReader = null;
        try {
            // создали читалку на основе файла
            reader = new FileReader(fileName);
            // создали буферизированную читалку
            bufferedReader = new BufferedReader(reader);
            // прочитали строку
            String line = bufferedReader.readLine();
            // пока к нам не пришла "нулевая строка"
            while (line != null) {
                // разбиваем ее по |
                String[] parts = line.split("\\|");
                // берем имя
                int id = Integer.parseInt(parts[0]);
                String name = parts[1];
                // берем возраст
                int age = Integer.parseInt(parts[2]);
                // берем статус о работе
                boolean isWorker = Boolean.parseBoolean(parts[3]);
                // создаем нового человека
                User newUser = new User(id, name, age, isWorker);
                // добавляем его в список
                users.add(newUser);
                // считываем новую строку
                line = bufferedReader.readLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            // этот блок выполнится точно
            if (reader != null) {
                try {
                    // пытаемся закрыть ресурсы
                    reader.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedReader != null) {
                try {
                    // пытаемся закрыть ресурсы
                    bufferedReader.close();
                } catch (IOException ignore) {
                }
            }
        }

        return users;
    }

    @Override
    public void save(User user) {
        Writer writer = null;
        BufferedWriter bufferedWriter = null;
        try {
            writer = new FileWriter(fileName, true);
            bufferedWriter = new BufferedWriter(writer);

            bufferedWriter.write(user.getID() + "|" + user.getName() + "|" + user.getAge() + "|" + user.isWorker());
            bufferedWriter.newLine();
            bufferedWriter.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException ignore) {
                }
            }
        }
    }

    @Override
    public List<User> findByAge(int age) {
        List<User> users = new ArrayList<>();
        Reader reader = null;
        BufferedReader bufferedReader = null;

        try {
            reader = new FileReader(fileName);
            bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                int id = Integer.parseInt(parts[0]);
                String name = parts[1];
                int ageUser = Integer.parseInt(parts[2]);
                boolean isWorker = Boolean.parseBoolean(parts[3]);
                if (age == ageUser) {
                    User newUser = new User(id, name, ageUser, isWorker);
                    users.add(newUser);
                }
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ignore) {
                }
            }
        }
        return users;
    }

    @Override
    public List<User> findByIsWorkerIsTrue() {
        List<User> users = new ArrayList<>();
        Reader reader = null;
        BufferedReader bufferedReader = null;

        try {
            reader = new FileReader(fileName);
            bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine();

            while (line != null) {
                String[] parts = line.split("\\|");
                int id = Integer.parseInt(parts[0]);
                String name = parts[1];
                int age = Integer.parseInt(parts[2]);
                boolean isWorker = Boolean.parseBoolean(parts[3]);
                if (isWorker) {
                    User newUser = new User(id, name, age, isWorker);
                    users.add(newUser);
                }
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ignore) {
                }
            }
        }
        return users;

    }
}


import java.util.List;

public interface UsersRepository {
    List<User> findAll();

    void save(User user);

    List<User> findByAge(int age);

    List<User> findByIsWorkerIsTrue();

    User findById(int ID);

    void update(User user);
}

public class User {
    private final int ID;
    private String name;
    private int age;
    private boolean isWorker;

    public User(int ID, String name, int age, boolean isWorker) {
        this.ID = ID;
        this.name = name;
        this.age = age;
        this.isWorker = isWorker;
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isWorker() {
        return isWorker;
    }

    public void setWorker(boolean worker) {
        isWorker = worker;
    }

    @Override
    public String toString() {
        return ("ID = " + ID + ", name = " + name + ", age = " + age + ", isWorker = " + isWorker);
    }

}

import java.util.List;

public class Main {
    /*Реализовать в классе UsersRepositoryFileImpl методы:

    User findById(int id);
    update(User user);

    Принцип работы методов:

    Пусть в файле есть запись:

    1|Игорь|33|true

    Где первое значение - гарантированно уникальный ID пользователя (целое число).

    Тогда findById(1) вернет объект user с данными указанной строки.

    Далее, для этого объекта можно выполнить следующий код:

    user.setName("Марсель");
    user.setAge(27);

    и выполнить update(user);

    При этом в файле строка будет заменена на 1|Марсель|27|true.

    Таким образом, метод находит в файле пользователя с id user-а и заменяет его значения.

    Примечания:
    Бесполезно пытаться реализовать замену данных в файле без полной перезаписи файла ;)
    */
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");
        User user = usersRepository.findById(5);
        System.out.println(user.toString());
        user.setAge(35);
        user.setName("Ольга");
        usersRepository.update(user);
    }
}
