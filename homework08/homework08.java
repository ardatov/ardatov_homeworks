public class homework08 {

    public static void main(String[] args) {

        String[] names = {"Нина", "Анатолий", "Иннокентий", "Ксения", "Людмила", "Алиса", "Иван", "Агафья", "Дарина", "Дмитрий"};
        double[] weights = {12.0, 90.5, 98.6, 82.7, 74.2, 56.3, 115.6, 59.1, 60.9, 103.3};

        Human[] people = new Human[names.length];

        for (int i = 0; i < names.length; i++) {
            people[i] = new Human(names[i], weights[i]);
        }

        for (int i = 0; i < people.length; i++) {
            double min = people[i].weight;
            int index = i;
            for (int j = i; j < people.length; j++){
                if (people[j].weight < min) {
                    min = people[j].weight;
                    index = j;
                }
            }
            Human tmp = people[i];
            people[i] = people[index];
            people[index] = tmp;
        }
        for (Human human: people){
            System.out.println(human.name + ": " + human.weight + " кг");
        }
    }
}