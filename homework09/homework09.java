public class Circle extends Ellipse{

    public Circle(int x, int y, int r) {
        super(x, y, r, r);
    }

    @Override
    public double getPerimetr() {
        return 2*Math.PI*r1;
    }
}

public class Ellipse extends Figure{
    protected int r1;
    private int r2;
    public Ellipse(int x, int y, int r1, int r2) {
        super(x, y);
        this.r1 = r1;
        this.r2 = r2;
    }

    @Override
    public double getPerimetr() {
        return 2*Math.PI*Math.sqrt((r1*r1+r2*r2)/2.0);
    }
}

public class Figure {
    protected int x;
    protected int y;
    public Figure(int x, int y){
        this.x = x;
        this.y = y;
    }
    public double getPerimetr(){
        return 0.0;
    }
}

public class Rectangle extends Figure{
    protected int a;
    private int b;
    public Rectangle(int x, int y, int a, int b) {
        super(x, y);
        this.a = a;
        this.b = b;
    }


    @Override
    public double getPerimetr() {
        return a * 2 + b * 2;
    }
}

public class Square extends Rectangle {

    public Square(int x, int y, int a) {
        super(x, y, a, a);
    }

    @Override
    public double getPerimetr() {
        return a*4;
    }
}

public class homework09 {
    public static void main(String[] args) {
        Ellipse m = new Ellipse(1,2,3, 4);
        System.out.println(m.getPerimetr());
        Circle c = new Circle(1,2,3);
        System.out.println(c.getPerimetr());
        Rectangle r = new Rectangle(1,2,3,4);
        System.out.println(r.getPerimetr());
        Square s = new Square(1,2,3);
        System.out.println(s.getPerimetr());
    }
}
